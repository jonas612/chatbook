FROM ruby:2.5.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN apt-get update -q && apt-get install -qy \
    texlive-full \
    python-pygments gnuplot \
    make git \
    && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/henningpohl/latex-emoji.git /root/texmf/tex/latex/latex-emoji
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN mkdir /chatbook
WORKDIR /chatbook
COPY Gemfile /chatbook/Gemfile
COPY Gemfile.lock /chatbook/Gemfile.lock
RUN bundle install
COPY . /chatbook
RUN bundle exec rake assets:precompile

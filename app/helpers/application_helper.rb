# frozen_string_literal: true

module ApplicationHelper
  # Adapted from https://github.com/lwe/simple_enum/blob/master/lib/simple_enum/view_helpers.rb
  # builds enum pairs of key and translation for select fields
  def enum_option_pairs(record, enum, encode_as_value = false)
    reader = enum.to_s.pluralize
    record = record.class unless record.respond_to?(reader)

    record.send(reader).map do |key, value|
      name = record.human_enum_name(enum, key) if record.respond_to?(:human_enum_name)
      name ||= translate_enum_key(enum, key)
      [name, encode_as_value ? value : key]
    end
  end

  def translate_enum(record, key)
    record.class.human_enum_name(key, record.public_send(key))
  end
  alias te translate_enum

  private

  def translate_enum_key(enum, key)
    defaults = [:"enums.#{enum}.#{key}", key.to_s.humanize]
    I18n.translate defaults.shift, default: defaults, count: 1
  end
end

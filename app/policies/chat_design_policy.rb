class ChatDesignPolicy < ApplicationPolicy
  
  def show?
    @record.chat_upload.user == @user
  end

  def update?
    @record.chat_upload.user == @user
  end

  def build_files?
    @record.chat_upload.user == @user
  end

  def tex_information?
    @record.chat_upload.user == @user
  end

  def download_tex?
    @record.chat_upload.user == @user
  end

  def download_pdf?
    @record.chat_upload.user == @user
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end

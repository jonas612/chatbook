class ChatUploadPolicy < ApplicationPolicy

  def index?
    @record.user == @user
  end
  
  def new?
    true
  end

  def create?
    true
  end

  def edit?
    update?
  end

  def update?
    @record.user == @user
  end

  def destroy?
    @record.user == @user
  end

  def processing?
    @record.user == @user
  end

  class Scope < Scope
    def resolve
      scope.where(user: @user)
    end
  end
end
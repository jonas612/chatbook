# frozen_string_literal: true

class ChatToLatex
  @@letters_to_escape = ['#', '$', '%', '_', '{', '}', '~', '&']

  # transforms a given string into LaTeX processable format
  def self.escape(line)
    
    # remove all backslashes
    # savest approach, but may destroy urls and character-base emojis (e.g. :-\)
    line.gsub! '\\', ""

    # save urls before special caracters are escaped
    list_of_urls = URI.extract(line, %w[http https])

    # escape reserved characters
    @@letters_to_escape.each do |letter|
      line.gsub! letter, '\\' + letter
    end

    # restore escaped urls and add tag
    list_of_urls.each do |url|
      escaped_url = String.new(url)
      @@letters_to_escape.each do |letter|
        escaped_url.gsub! letter, '\\' + letter
      end
    line.gsub! escaped_url, '\\url{' + escaped_url + '}'
    end

    # replace other reserved letters
    line.gsub! '"', "''"
    line.gsub! "\„", "''"
    line.gsub! '&', I18n.t("and", scope: :dict) # somehow not escapable
    line.gsub! '^', "" # causes random errors

    # special case "\" in the end of the line has to be removed (sorry!)
    line = line.chomp("\\")

    # replaces facebook stickers with corrosponding emojis
    line = replace_facebook_stickers(line)

    # deal with emojis
    line = escape_emojis(line)

    # return line
    line
  end

  private
  # escapes supported emojis
  def self.escape_emojis(line)

    # open file with processable emojis
    emoji_in_latex = File.open('app/lib/emoji_data/emoji_latex.txt', 'r')
    emoji_in_latex.each_line do |emoji|

      # remove linebreak from txt file
      emoji = emoji.chomp

      # skip if emoji is not included in the line
      next unless line.include? emoji

      # emojis hexadecimal codepoint filled up with zeros
      escaped_emoji = emoji.each_codepoint.map { |n| n.to_s(16).rjust(4, '0') }

      # build emoji string from codepoints (-> has to equal filename from latex-emoji)
      emoji_string = escaped_emoji.join('-')
      emoji_string = emoji_string.upcase

      # adjusting some errors in latex-emoji filenaming 
      emoji_string.slice!("-FE0F") # remove unecessary additional codepoint FE0F
      emoji_string = "1F3C7" if emoji_string.include? "1F3C7" # missing diversity for rider emoji

      # replace all occurences of this emoji with latex_emoji tag
      line.gsub! emoji, '\\emoji{' + emoji_string + '}'
    end
    # returns line
    line
  end

  # replaces facebook stickers with common emojis or deletes them
  def self.replace_facebook_stickers(line)
    # Facebook ThumbsUp
    line.tr! "\u{f0000}", "\u{1f44d}"
    line
  end
end

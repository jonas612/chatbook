# frozen_string_literal: true

class ChatDesign < ApplicationRecord

  #scopes
  scope :active, -> { where(status: 'active') }
  
  # associations
  belongs_to :chat_upload

  # file associations
  has_one_attached :tex_file
  has_one_attached :pdf_file

  # enum declaration
  enum processing_status: %i[inactive processing success error]
  enum page_size: %i[a4 a5]
  enum page_outline: %i[table_design flow_design bubbles_design]
  enum color_set: [:whatsapp, :pastel, :bright, :bootstrap, :instagram]
  enum font: %i[fira_sans chancery courier]

  # validation steps for wizard
  # step: create_design
  with_options if: :active_or_create_design? do |design|
    design.validates :name, presence: true
  end
  # step: basic_configuration
  with_options if: :active_or_basic_configuration? do |design|
    design.validates :my_user_name, presence: true
    design.validate :any_chat_messages_in_period
    # validates :period_from, presence: true
    # validates :period_to, presence: true
  end
  # step: style_configuration
  with_options if: :active_or_style_configuration? do |design|
    design.validates :page_size, presence: true
    design.validates :page_outline, presence: true
    design.validates :column_count, presence: true
    design.validates :font, presence: true
    design.validates :color_set, presence: true
  end

  # validation flags for wizard steps
  def active?
    status == 'active'
  end

  def active_or_create_design?
    status.include?('create_design') || active?
  end

  def active_or_basic_configuration?
    status.include?('basic_configuration') || active?
  end

  def active_or_style_configuration?
    status.include?('style_configuration') || active?
  end

  # custom validation for messages in selected time period
  def any_chat_messages_in_period
    errors[:period_from] << 'No messages in the selected period' unless chat_messages_in_period.any?
    errors[:period_to] << 'No messages in the selected period' unless chat_messages_in_period.any?
  end


  # build list of available bg_images
  def self.get_background_images
    bg_images = Dir.entries("latex/backgrounds/").sort
    bg_images.delete(".")
    bg_images.delete("..")
    bg_images.unshift("- none -")
    return bg_images
  end

  # returns the chat messages that are in the specified period
  def chat_messages_in_period
    if period_from.nil? && period_to.nil?
      messages = chat_upload.chat_messages.all
    elsif period_from.present? && period_to.nil?
      messages = chat_upload.chat_messages.where("timestamp >= ?", period_from)
    elsif period_from.nil? && period_to.present?
      messages = chat_upload.chat_messages.where("timestamp <= ?", period_to)
    elsif period_from.present? && period_to.present?
      messages = chat_upload.chat_messages.where(timestamp: (period_from..period_to))
    end
    # workaround for a problem with whatsapps timestamps do not include seconds
    # in order to show whatsapp messages in the correct order they have to ordered
    # by the time of their creation (file is processed line by line)
    # returns the correctly sorted version
    if chat_upload.whatsapp?
      messages.order(:created_at)
    else
      messages.order(:timestamp)
    end
  end
end

# frozen_string_literal: true

class ChatMessage < ApplicationRecord
  
  # associations
  belongs_to :chat_upload

  # file associations
  has_one_attached :content_media

  enum msg_type: [:user_msg, :system_msg]

  # validations
  validates :sender, presence: true
  validates :timestamp, presence: true
  validates :chat_upload, presence: true
end

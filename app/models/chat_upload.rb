# frozen_string_literal: true

class ChatUpload < ApplicationRecord
  
  # scopes
  scope :existing, -> { where.not(processing_status: 'deleted') }
  
  # asscociations
  belongs_to :user
  has_many :chat_messages, dependent: :delete_all
  has_many :chat_designs, dependent: :delete_all

  # file associations
  has_one_attached :file_raw

  # enum declarations
  enum messenger_type: %i[whatsapp fbmessenger]
  enum processing_status: %i[processing success error deleted]

  #validations
  validates :name, presence: true
  validates :messenger_type, presence: true
  validate :file_raw_validation
  validates :user, presence: true
  validates :file_raw, blob: { content_type: ['application/json', 'text/plain'] }

  # instance methods
  def file_raw_validation
    errors[:file_raw] << 'No file selected' unless file_raw.attached?
  end

  def groupchat?
    chat_messages.where.not(sender: 'System').pluck(:sender).uniq.length > 2
  end

  def participants
    chat_messages.where.not(sender: 'System').pluck(:sender).uniq
  end

  def message_count
    chat_messages.count
  end
end

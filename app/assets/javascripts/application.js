// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require popper
//= require bootstrap
//= require foundation-datetimepicker
//= require activestorage
//= require_tree .

// Bootstrap tooltip and popover initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
})

// Bootstrap tooltip and popover hide
// called before AJAX refresh to prevent from stuck popovers
function destroyPopovers() {
  $('[data-toggle="tooltip"]').tooltip('hide')
  $('[data-toggle="popover"]').popover('hide')
}

// Reinitialize tooltip and popover after ajax refresh
$(document).ajaxComplete(function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});

// Foundation Datepicker 
$(function(){
  $('.datetime').fdatetimepicker({
    format: 'dd.mm.yyyy hh:ii',
    disableDblClickSelection: true,
    language: 'en',
    pickTime: true
  });
});

// Bootstrap collapse
$('.collapse').collapse()

$('#collapseWhatsapp').collapse()


// calls action refreshing the partial for processing entries
function refreshProcessingChatDesignPartial(refresh_id, page) {
  destroyPopovers(); // prevent from stuck popovers
  $.ajax({
    url: "chat_designs",
    data: {
      page: page,
      refresh_id: refresh_id
    }
 })
}
function refreshProcessingChatUploadPartial(refresh_id, page) {
  destroyPopovers(); // prevent from stuck popovers
  $.ajax({
    url: "chat_uploads",
    data: {
      page: page,
      refresh_id: refresh_id
    }
 })
}
function refreshProcessingViewChatUploadPartial() {
  destroyPopovers(); // prevent from stuck popovers
  $.ajax({
    url: "processing",
 })
}
function refreshProcessingViewChatDesignPartial() {
  destroyPopovers(); // prevent from stuck popovers
  $.ajax({
    url: "processing",
 })
}

// wizard preview functions
function refreshPreview(element,value,columns) {
  $.ajax({
    url: "style_configuration/refresh_preview",
    data: {
      element: element,
      value: value,
      columns: columns
    }
  })
}
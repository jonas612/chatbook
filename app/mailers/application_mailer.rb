class ApplicationMailer < ActionMailer::Base
  # mailers are currently unused
  default from: 'from@example.com'
  layout 'mailer'
end

# frozen_string_literal: true

class FilesController < ApplicationController
  def download_tex
    # checks if design and upload can be acquired from the database
    return design_not_found unless ChatDesign.exists?(params[:chat_design_id])

    @chat_design = ChatDesign.find(params[:chat_design_id])
    return tex_file_not_found unless @chat_design.tex_file.attached?

    # checks if currently logged-in user is authorized to see the data
    authorize @chat_design

    # executes the archive generation service and offers result for download
    result = Files::ZipTexService.new(@chat_design, @user).execute
    if result.success?
      send_data(result.subject.read, filename: 'chat.zip', type: 'application/zip', disposition: 'attachment')
    else
      flash[:alert] = 'Error creating tex archive: ' + result.error.to_s
    end
  end

  def tex_information
    # checks if design and upload can be acquired from the database
    return design_not_found unless ChatDesign.exists?(params[:chat_design_id])

    @chat_design = ChatDesign.find(params[:chat_design_id])
    return tex_file_not_found unless @chat_design.tex_file.attached?

    # checks if currently logged-in user is authorized to see the data
    authorize @chat_design
  end

  def download_pdf
    # checks if design and upload can be acquired from the database
    return design_not_found unless ChatDesign.exists?(params[:chat_design_id])

    @chat_design = ChatDesign.find(params[:chat_design_id])
    return tex_file_not_found unless @chat_design.tex_file.attached?

    # checks if currently logged-in user is authorized to see the data
    authorize @chat_design

    # fetches the pdf file from active storage and offers it for download
    send_data(@chat_design.pdf_file.download, filename: 'chat.pdf', type: 'application/pdf', disposition: 'inline')
  end

  private

  def design_not_found
    flash[:alert] = 'Design not found.'
    redirect_to chat_uploads_path
  end

  def tex_file_not_found
    flash[:alert] = 'Tex-File not found. Try to save design to create it.'
    redirect_to chat_upload_chat_designs_path(@chat_design.chat_upload)
  end
end

# frozen_string_literal: true

class ChatUploadsController < ApplicationController
  def new
    @chat_upload = ChatUpload.new
    authorize @chat_upload
  end

  def create
    skip_authorization # does not need to be authorized
    result = ChatUploads::CreateService.new(chat_upload_params, current_user).execute
    if result.success?
      redirect_to chat_upload_processing_path(result.subject)
    else
      @chat_upload = result.subject
      flash[:alert] = "Error: " + result.error.to_s
      render 'new'
    end
  end

  def index
    # get upload from database
    @chat_uploads = policy_scope(ChatUpload).existing.order(:created_at)
    
    # for refreshing row in processing viw ajax the specific row is loaded
    @chat_upload_to_refresh = @chat_uploads.find(params[:refresh_id]) if params[:refresh_id].present?

    # paginate uploads and get current page
    @chat_uploads = @chat_uploads.page params[:page]
    @current_page = @chat_uploads.current_page

    respond_to do |format|
      format.js
      format.html
    end
  end

  def destroy
    @chat_upload = ChatUpload.find(params[:id])
    authorize @chat_upload

    # set deleted flag at upload
    @chat_upload.update(processing_status: :deleted)

    # call asynchronous worker for upload deleting
    UploadDestroyWorker.perform_async(@chat_upload.id, current_user.id)

    # notify user and redirect
    flash[:notice] = "Upload successfully deleted!"
    redirect_to chat_uploads_path
  end

  def processing
    @chat_upload = ChatUpload.find(params[:id])
    authorize @chat_upload

    # redirect to design creation when processing is complete
    # redirection when request comes via ajax
    render :js => "window.location = '/chat_uploads/#{@chat_upload.id}/chat_designs/build/wizards'" if request.xhr? && @chat_upload.success?
    # redirect if a user accesses the page for a finished upload
    redirect_to "/chat_uploads/#{@chat_upload.id}/chat_designs/build/wizards" if !request.xhr? && @chat_upload.success?

    respond_to do |format|
      format.js
      format.html
    end
  end

  private

  def chat_upload_params
    params.require(:chat_upload).permit(:name, :description, :messenger_type, :file_raw)
  end
end

# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def index
    # index does not have to be authorized
    skip_policy_scope
  end

  def export_help
    skip_authorization
  end

  def imprint
    skip_authorization
    load_owner_data
  end

  def privacy_policy
    skip_authorization
    load_owner_data
  end

  private

  # loads owner data from the ENV variables to be displayed in legal pages
  def load_owner_data
    @owner_name = ENV['SITE_OWNER_NAME']
    @owner_street = ENV['SITE_OWNER_STREET']
    @owner_city = ENV['SITE_OWNER_CITY']
    @owner_email = ENV['SITE_OWNER_EMAIL']
    @owner_phone = ENV['SITE_OWNER_PHONE']
  end
end

# frozen_string_literal: true

class ChatDesignsController < ApplicationController
  def index
    skip_policy_scope # chat_upload is being authorized
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_designs = ChatDesign.where(chat_upload: params[:chat_upload_id], status: :active).order(:created_at).page params[:page]
    @current_page = @chat_designs.current_page
    
    # for refreshing row in processing via ajax script the specific row is loaded
    @chat_design_to_refresh = @chat_upload.chat_designs.find(params[:refresh_id]) if params[:refresh_id].present?

    respond_to do |format|
      format.js
      format.html
    end
  end

  def new
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = ChatDesign.new
  end

  def create
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = @chat_upload.chat_designs.create(chat_design_params.merge(status: 'active'))
    if @chat_design.save
      redirect_to chat_upload_chat_design_build_files_path(@chat_upload, @chat_design)
    else
      render 'new'
    end
  end

  def edit
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = @chat_upload.chat_designs.find(params[:id])
  end

  def update
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = @chat_upload.chat_designs.find(params[:id])
    if @chat_design.update(chat_design_params)
      redirect_to chat_upload_chat_design_build_files_path(@chat_upload, @chat_design)
    else
      render 'edit'
    end
  end

  def destroy
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = @chat_upload.chat_designs.find(params[:id])
    @chat_design.destroy

    flash[:notice] = t('.design_successfully_deleted')
    redirect_to chat_upload_chat_designs_path
  end

  def build_files
    # loads the chat design and calls the worker for file creation
    @chat_design = ChatDesign.find(params[:id])
    authorize @chat_design
    reset_processing_status
    LatexCreateWorker.perform_async(@chat_design.id, current_user.id)
    redirect_to chat_upload_chat_design_processing_path
  end


  def processing
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = @chat_upload.chat_designs.find(params[:id])

    respond_to do |format|
      format.js
      format.html
    end
  end

  private

  def chat_design_params
    params.require(:chat_design).permit(:name, :my_user_name, :period_from, :period_to, :page_size, :page_outline, :column_count, :color_set, :font, :bg_image, :chat_upload_id)
  end

  # resets all processing attributes for a given ChatDesign
  def reset_processing_status
    @chat_design.update(processing_status: :processing)
    @chat_design.update(processing_error: nil)
    @chat_design.update(processing_progress: 0)
  end
end

# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery

  before_action :load_environment

  # force to authorize controller actions with pundit unless special cases
  after_action :verify_authorized, except: :index, unless: :skip_authorization?
  after_action :verify_policy_scoped, only: :index, unless: :skip_authorization?

  # recue from pundit authorization error
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # basic auth for staging system
  http_basic_authenticate_with name: ENV['SITE_USERNAME'], password: ENV['SITE_PASSWORD'], if: :authenticate_site_access?

  # do not authorize with pundit in users controller
  def skip_authorization?
    devise_controller?
  end

  # valuates if site access should be restricted due to ENV config
  def authenticate_site_access?
    @site_username.present? && @site_password.present?
  end

  # loads ENV variables to be used within views
  def load_environment
    @local_mode = ENV['LOCAL_MODE']
    @site_username = ENV['SITE_USERNAME']
    @site_password = ENV['SITE_USERNAME']
  end

  # i18n related functions
  # before_action :set_locale

  # def set_locale
  #   I18n.locale = params[:locale] || I18n.default_locale
  # end

  # def default_url_options
  #   { locale: I18n.locale }
  # end

  private

  # redirect an unauthorized user
  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end

end

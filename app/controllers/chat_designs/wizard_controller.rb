class ChatDesigns::WizardController < ApplicationController
  include Wicked::Wizard

  steps :create_design, :basic_configuration, :style_configuration
  
  def show
    @chat_design = ChatDesign.find(params[:chat_design_id])
    authorize @chat_design
    @chat_upload = @chat_design.chat_upload

    # wizard steps
    case steps
    when :create_design
      
    when :basic_configuration

    when :style_configuration

    end
    render_wizard
  end

  def update
    # writes to the intermediate object and sets status accordingly
    @chat_design = ChatDesign.find(params[:chat_design_id])
    authorize @chat_design
    @chat_upload = @chat_design.chat_upload
    params[:chat_design][:status] = step.to_s
    params[:chat_design][:status] = 'active' if step == steps.last
    params[:chat_design][:bg_image] = nil if params[:chat_design][:bg_image] == '- none -'
    @chat_design.update_attributes(chat_design_params)
    render_wizard @chat_design
  end

  def create
    # create an inital empty object to be completed by the wizard
    @chat_upload = ChatUpload.find(params[:chat_upload_id])
    authorize @chat_upload
    @chat_design = ChatDesign.create!(chat_upload: @chat_upload)
    # deletes the object, if it is not finished in one hour
    DesignWizardCleanupWorker.perform_in(60.minutes, @chat_design.id, current_user.id)
    redirect_to chat_upload_chat_design_wizard_path(@chat_upload, @chat_design, steps.first)
  end

  def refresh_preview
    # ajax refresh action: 
    # reads data from the selected customization options and loads data accordingly
    # in order to refresh the preview in the 3rd wizard step
    skip_authorization

    @element = params[:element]
    @value = params[:value]
    @columns = params[:columns]

    if @element == "color_set"
      @color_sets = Dir.entries("app/assets/images/preview_color_sets").sort
      @color_sets.delete('..')
      @color_sets.delete('.')
    end

    respond_to do |format|
      format.js
    end
  end

  private

  def chat_design_params
    params.require(:chat_design).permit(:name, :my_user_name, :period_from, :period_to, :page_size, :page_outline, :column_count, :color_set, :font, :bg_image, :chat_upload_id, :status)
  end

  # path the wizard redirects to after the last step is completed
  def finish_wizard_path
    chat_upload_chat_design_build_files_path(@chat_upload, @chat_design)
  end
end

class DesignWizardCleanupWorker
  include Sidekiq::Worker

  # deletes an unfinished design, if the user drops out of the wizard
  # is enqueued by the controller to run 60mins after wizard started
  def perform(chat_design_id, user_id)
    @chat_design = ChatDesign.where(id: chat_design_id).first
    @user = User.find(user_id)

    # checks if chat design is in the db and deletes 
    # if it has not been finished in the wizard
    if @chat_design.present?
      @chat_design.destroy unless @chat_design.active?
    end
  end
end

class LatexCompileWorker
  include Sidekiq::Worker

  def perform(chat_design_id, user_id)
    @chat_design = ChatDesign.where(id: chat_design_id).first
    @user = User.find(user_id)

    # check if chat_design is present
    return if @chat_design.nil?

    # executes compile service and fetches results
    result = Latex::CompileService.new(@chat_design, @user).execute
    
    # handles result object
    if result.success?
      @chat_design.update(processing_status: :success)
    else
      @chat_design.update(processing_status: :error)
      @chat_design.update(processing_error: "Error in .pdf generation: In some rare occasions the LaTeX compiler cannot deal with certain inputs. Download the .tex files and try to debug yourself.")
    end
  end
end

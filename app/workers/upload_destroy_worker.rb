class UploadDestroyWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0

  # savely destroys upload records
  def perform(chat_upload_id, user_id)
    @chat_upload = ChatUpload.find(chat_upload_id)
    @user = User.find(user_id)

    ChatUpload.transaction do
      @chat_upload.destroy
    end

  end
end

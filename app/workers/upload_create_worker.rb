class UploadCreateWorker
  include Sidekiq::Worker

  def perform(chat_upload_id, user_id)
    @chat_upload = ChatUpload.find(chat_upload_id)
    @user = User.find(user_id)

    # executes parser dependant on the selected messenger_type
    case @chat_upload.messenger_type
    when "whatsapp"
      parseResult = ChatMessages::ParseWhatsappService.new(@chat_upload, @user).execute
    when "fbmessenger"
      parseResult = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    else
      puts('Messengertype unsupported')
    end

    # handles results
    if parseResult.success?
      @chat_upload.update(processing_status: :success)
    else
      @chat_upload.update(processing_status: :error)
      @chat_upload.update(
        processing_error: "An error ocurred during message processing. Have you selected the correct file and messenger?"
        )
    end
  end
end

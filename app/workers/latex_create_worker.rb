class LatexCreateWorker
  include Sidekiq::Worker

  def perform(chat_design_id, user_id)
    @chat_design = ChatDesign.where(id: chat_design_id).first
    @user = User.find(user_id)

    # check if chat_design is present
    return if @chat_design.nil?

    # executes latex create service and fetches results
    result = Latex::CreateService.new(@chat_design, @user).execute

    # handles result object   
    if result.success?
      # no success flag, since the other worker has to finish first
      LatexCompileWorker.perform_async(@chat_design.id, @user.id)
    else
      @chat_design.update(processing_status: :error)
      @chat_design.update(processing_error: "Error in .tex generation: Try another combination of options for your Design.")
    end
  end
end

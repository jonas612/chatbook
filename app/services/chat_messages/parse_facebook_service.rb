# frozen_string_literal: true

require 'json'

module ChatMessages
  class ParseFacebookService < ::BaseService
    def initialize(chat_upload, user)
      super(user)

      @chat_upload = chat_upload
    end

    def execute
      # return error if executed for the wron messenger type
      return error("Wrong Messenger Format") unless @chat_upload.messenger_type == "fbmessenger"

      # remove existing messages
      clearExistingEntries

      # load data from JSON import and parse
      chat_hash = ActiveSupport::JSON.decode(@chat_upload.file_raw.download)
      message_hash = chat_hash['messages']
      message_hash.each do |message|
        parseAndSaveMessage(message)
      end
      success(@chat_upload)
    rescue StandardError => exception
      error(exception)
    end

    private

    def parseAndSaveMessage(message)
      # message type distinction
      chat_message = ChatMessage.new
      chat_message.msg_type = if message['type'] != 'Generic'
                                :system_msg
                              else
                                :user_msg
                              end
      if message['sender_name'].present?
        chat_message.sender = message['sender_name'].encode('iso-8859-1').force_encoding('utf-8')
      else
        chat_message.sender = '<No Sender>'
      end
      if message['content'].present?
        chat_message.content_text = message['content'].encode('iso-8859-1').force_encoding('utf-8')
      else
        chat_message.content_text = " "
      end
      chat_message.timestamp = DateTime.strptime((message['timestamp_ms']).to_s, '%Q')
      @chat_upload.chat_messages << chat_message
      chat_message.save
    end

    def clearExistingEntries
      chat_messages = ChatMessage.where(chat_upload: @chat_upload)
      chat_messages.each(&:destroy)
    end
  end
end

# frozen_string_literal: true

module ChatMessages
  class ParseWhatsappService < ::BaseService
    def initialize(chat_upload, user)
      super(user)

      @chat_upload = chat_upload
    end

    # Adapted from https://github.com/Pustur/whatsapp-chat-parser/blob/master/src/parser.js
    @@regexParser = /\[?(?<date>\d{1,2}[-\/.]\d{1,2}[-\/.]\d{2,4}),? (?<time>\d{1,2}[.:]\d{1,2}(?:[.:]\d{1,2})?)(?: (?<ampm>[ap]\.?m\.?))?\]?(?: -|:)? (?<sender>.+?): (?<content_text>(?:.|\s)*)/i
    @@regexParserSystem = /\[?(?<date>\d{1,2}[-\/.]\d{1,2}[-\/.]\d{2,4}),? (?<time>\d{1,2}[.:]\d{1,2}(?:[.:]\d{1,2})?)(?: (?<ampm>[ap]\.?m\.?))?\]?(?: -|:)? (?<content_text>(?:.|\s)+)/i
    @@regexStartsWithDateTime = /\[?(\d{1,2}[-\/.]\d{1,2}[-\/.]\d{2,4}),? (\d{1,2}[.:]\d{1,2}(?:[.:]\d{1,2})?)(?: ([ap]\.?m\.?))?\]?/i

    def execute
      # return error if executed for the wrong messenger type
      return error("Wrong Messenger Format") unless @chat_upload.messenger_type == "whatsapp"

      # delete existing entries
      clearExistingEntries

      # use regex to idetify the type of message and call corropsonding function
      @chat_upload.file_raw.download.each_line do |line|
        if !@@regexParser.match(line)
          if @@regexStartsWithDateTime.match?(line)
            # System Message: Timestamp and Message, but no Sender
            parseAndSaveMessage(line, true)
          else
            # Line breaked message: Message Only (Belongs to prior message)
            add_to_previous(line)
          end
        else
          # Normal message: Timestamp, Sender, Message
          parseAndSaveMessage(line)
        end
      end
      success(@chat_upload)
    rescue StandardError => exception
      error(exception)
    end

    private

    def parseAndSaveMessage(message, system_msg = false)
      if system_msg
        captures = Hash[@@regexParserSystem.names.zip(message.force_encoding('utf-8').match(@@regexParserSystem).captures)]
        message_hash = { msg_type: :system_msg }
        message_hash[:sender] = 'System'
      else
        captures = Hash[@@regexParser.names.zip(message.force_encoding('utf-8').match(@@regexParser).captures)]
        message_hash = { msg_type: :user_msg }
        message_hash[:sender] = captures['sender']
      end
      
      # compute timestamp from regex captures and raise error if it fails
      timestamp = parseDate(captures)
      raise 'Timestamp error detected: ' + captures['date'].to_s + ' ' + captures['time'].to_s + ' ' + captures['ampm'].to_s if timestamp.nil?

      # deletes possible nullbytes in message
      captures['content_text'].delete!("\0")

      # build the rest of the data hash and save line to db
      message_hash[:content_text] = captures['content_text']
      message_hash[:timestamp] = timestamp
      message_hash[:chat_upload] = @chat_upload
      chat_message = ChatMessage.new(message_hash)
      chat_message.save!
    end

    def parseDate(captures)
      # chronic fails on dot-seperated dates
      date = captures['date'].to_s
      date.tr! '.', '/'
      Chronic.parse(date + ' ' + captures['time'].to_s + ' ' + captures['ampm'].to_s)
    end

    def add_to_previous(message)
      # load the last written message and attach the current line
      prev_message = ChatMessage.where(chat_upload: @chat_upload).order(created_at: :desc).first
      prev_message.content_text.concat(message.force_encoding('utf-8'))
      prev_message.save
    end

    def clearExistingEntries
      # cycle through all attached messages and delete them
      chat_messages = ChatMessage.where(chat_upload: @chat_upload)
      chat_messages.each(&:destroy)
    end
  end
end

module ResultWithError
  # makes the result object testable with .success?
  def success?
    error.nil?
  end
end
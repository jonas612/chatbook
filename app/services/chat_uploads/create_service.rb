# frozen_string_literal: true

module ChatUploads
  class CreateService < ::BaseService
    def initialize(params, user)
      super(user)
      @params = params
    end

    def execute
      # Save the chat_upload
      @chat_upload = ChatUpload.new(@params.merge(user: user, processing_status: :processing))
      if !@chat_upload.save
        return error('Error while saving your Upload.', @chat_upload)
      end
      
      # message processing ansynchronous from here on
      UploadCreateWorker.perform_async(@chat_upload.id, @user.id)
      
      return success(@chat_upload)

    rescue StandardError => exception
      return error(exception,@chat_upload)
    end
  end
end

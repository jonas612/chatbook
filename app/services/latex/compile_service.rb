# frozen_string_literal: true

module Latex
  class CompileService < ::BaseService
    def initialize(chat_design, user)
      super(user)
      @chat_design = chat_design
    end

    def execute
      # wrong argument handling
      raise "Design not found" unless @chat_design.present?

      # loads tex file from active storage
      @tex_file = @chat_design.tex_file

      # writes configuration hash for rails-latex
      config = Hash.new
      config[:arguments] = ['-shell-escape', '-interaction=batchmode']
      # config[:command] = "xelatex"
      
      # executes texlive with the given file and configuration
      @pdf_bin = LatexToPdf.generate_pdf(@tex_file.download, config)
      
      # writes result to a temporary file
      @pdf_file = Tempfile.new
      @pdf_file.write(@pdf_bin)

      # saves file
      @pdf_file.rewind
      @chat_design.pdf_file.purge
      @chat_design.pdf_file.attach(io: @pdf_file, filename: "chat.pdf", content_type: "application/pdf")

      success(@pdf_file)
    rescue StandardError => exception
      error(exception, @pdf_file)
    end
  end
end

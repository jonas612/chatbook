# frozen_string_literal: true

module Latex
  class CreateService < ::BaseService
    def initialize(chat_design, user)
      super(user)
      @chat_design = chat_design
      @chat_upload = chat_design.chat_upload
    end

    def execute
      raise "Design not found" unless @chat_design.present?

      # delete existing files
      @chat_design.tex_file.purge
      @chat_design.pdf_file.purge

      # select messages according to selected period, all messages if no period has been selected
      @chat_messages = @chat_design.chat_messages_in_period

      # initialize progress progress to zero
      @chat_design.update(processing_progress: 0)
      @chat_message_count = @chat_messages.count
      
      # decide which funtion to use according to selected outline
      case @chat_design.page_outline.to_sym
      when :table_design
        @tex_file = latex_table
      when :flow_design
        @tex_file = latex_flow
      when :bubbles_design
        @tex_file = latex_bubbles
      else
        raise "Corrupted Design: Outline missing"
      end

      # save file
      @tex_file.rewind
      @chat_design.tex_file.attach(io: @tex_file, filename: "chat.tex", content_type: "application/x-tex")

      # terminate service
      success(@chat_design)
    rescue StandardError => exception
      error(exception, @chat_design)
    end

    private

    def latex_table
      tex_file = Tempfile.new
      File.open(tex_file,"w+") do |f|
        # ### preamble
        f << "\\documentclass{table_design} \n"
        f << "\ \n"

        # set bg_image
        f << "\\graphicspath{{latex/}}\n"
        f << eval_bg_image
        
        # set page_size
        f << eval_page_size

        # set font
        f << eval_font

        # set title parameters
        f << "\n"
        f << "\\title{" + ChatToLatex.escape(@chat_upload.name) + "}\n"
        f << "\\author{" + ChatToLatex.escape(@chat_upload.description) + "}\n" if @chat_upload.description.present?
        f << "\n"

        # ### document body
        f << "\\begin{document}\n"
        f << "\\begin{center}\n"
        f << "\n"
        
        # table of messages
        f << "\\begin{longtable}{|p{.15\\textwidth}|p{.15\\textwidth}|p{.60\\textwidth}|}\n"
        f << "\\hline\n" 
        f << "Time & User & Text \\\\ \\hline \n"
        f << "\\endhead"

        # table body
        @chat_messages.each_with_index do |message,index|
          if message.msg_type!="system_msg"

            # track progress
            track_progress(index)
            
            escaped_chat_message = ChatToLatex.escape(message.content_text)
            f.puts(message.timestamp.strftime("%d/%m/%y - %H:%M").to_s + "&" + message.sender + "&" + escaped_chat_message + "\\\\") 
          end
        end

        f << "\\hline\n"
        f << "\\end{longtable}\n"
        f << "\n"

        # closing tags
        f << "\\end{center}"
        f << "\\end{document}"
      end
      return tex_file
    end

    def latex_flow
      tex_file = Tempfile.new
      File.open(tex_file,"w+") do |f|
        # ### preamble
        f << "\\documentclass{flow_design}\n"
        f << "\n"
        
        # set bg_image
        f << "\\graphicspath{{latex/}}\n"
        f << eval_bg_image
        
        # set page_size
        f << eval_page_size

        # set font
        f << eval_font

        # set title parameters
        f << "\n"
        f << "\\title{ " + ChatToLatex.escape(@chat_upload.name) + " }\n"
        f << "\\author{ " + ChatToLatex.escape(@chat_upload.description) + " }\n" if @chat_upload.description.present?
        f << "\n"

        # ### document body
        f << "\\begin{document}\n"
        
        # set column number
        f << eval_column_count
        f << "\n"
        
        # messages
        last_day=""
        @chat_messages.each_with_index do |message,index|
          if message.msg_type!="system_msg"

            # track progress
            track_progress(index)
            
            # checks if the day has changed in comparison to last msg
            day = message.timestamp.strftime("%d")
            f.puts(flow_new_day(message)) unless day.eql?(last_day)
            last_day = message.timestamp.strftime("%d")
            
            if message.sender == @chat_design.my_user_name
              f.puts(flow_outgoing_message(message))
            else
              f.puts(flow_incoming_message(message))
            end
          end
        end

        f << "\n"

        # close tags
        f << eval_column_count(true)
        f << "\\end{document}"
      end
      return tex_file
    end

    def latex_bubbles
      tex_file = Tempfile.new
      File.open(tex_file,"w+") do |f|
        # ### preamble
        f << "\\documentclass{bubbles_design}\n"
        f << "\n"
        
        # set bg_image
        f << "\\graphicspath{{latex/}}\n"
        f << eval_bg_image
        
        # set page_size
        f << eval_page_size

        # set font
        f << eval_font
        
        # set title parameters
        f << "\n"
        f << "\\title{ " + ChatToLatex.escape(@chat_upload.name) + " }\n"
        f << "\\author{ " + ChatToLatex.escape(@chat_upload.description) + " }\n" if @chat_upload.description.present?
        f << "\n"

        # ### document body
        f << "\\begin{document}\n"
        
        f << eval_column_count
        f << "\n"
        
        # messages
        @chat_messages.each_with_index do |message,index|
          if message.msg_type!="system_msg"

            # track progress
            track_progress(index)

            if message.sender == @chat_design.my_user_name
              f.puts(bubbles_outgoing_message(message))
            else
              f.puts(bubbles_incoming_message(message))
            end
          end
        end

        f << "\n"

        # closing tags
        f << eval_column_count(true)
        f << "\\end{document}"
      end
      return tex_file
    end

    # evaluate page_size
    def eval_page_size
      case @chat_design.page_size.to_sym 
      when :a4
        return "\\usepackage[a4paper, total={7in, 10in}]{geometry}\n"
      when :a5
        return "\\usepackage[a5paper, total={5in, 7in}]{geometry}\n"
      end
    end

    # evaluate font
    def eval_font
      case @chat_design.font.to_sym
      when :fira_sans
        return "\\usepackage[sfdefault]{FiraSans}\n"
      when :courier
        return "\\usepackage{courier} \n"
      when :chancery
        return "\\usepackage{chancery} \n"
      end
    end

    # evaluate column count
    def eval_column_count(end_tag = false)
      return "" if @chat_design.column_count < 2
      if end_tag
        return "\\end{multicols}\n"
      else
        return "\\begin{multicols}{" + @chat_design.column_count.to_s + "}\n"
      end
    end

    # evaluate bg_image    
    def eval_bg_image
      return if @chat_design.bg_image.blank?
      return ("\\usepackage{eso-pic}
        \\AddToShipoutPictureBG{%
          \\AtPageLowerLeft{\\includegraphics[width=\\paperwidth,height=\\paperheight]{backgrounds/" + @chat_design.bg_image + "}}
          }\n")
    end

    # generates a color tag with tag incoming/outgoing and usernumber
    def generate_color_tag(message, outgoing = false)
      if outgoing
        return @chat_design.color_set + "_outgoing"
      else
        senders = @chat_design.chat_upload.participants
        senders.delete(@chat_design.my_user_name)
        # generate color for each user (4 options)
        color_id = senders.index(message.sender) % 4
        return @chat_design.color_set + "_incoming_" + color_id.to_s
      end
    end

    def bubbles_outgoing_message(message)
      escaped_chat_message = ChatToLatex.escape(message.content_text)
      return "\\chatoutgoing{" + message.timestamp.strftime("%d/%m/%Y - %H:%M") + " " + message.sender + "}{" + generate_color_tag(message,true) + "}{" + escaped_chat_message + "}"
    end

    def bubbles_incoming_message(message)
      escaped_chat_message = ChatToLatex.escape(message.content_text)
      "\\chatincoming{" + message.timestamp.strftime("%d/%m/%Y - %H:%M") + " " + message.sender + "}{" + generate_color_tag(message) + "}{" + escaped_chat_message + "}"
    end

    def flow_outgoing_message(message)
      escaped_message = ChatToLatex.escape(message.content_text)
      return "\\chatoutgoing{" + message.timestamp.strftime("%H:%M") + " " + message.sender + "}{" + generate_color_tag(message,true) + "}{" + escaped_message + "}"
    end

    def flow_incoming_message(message)
      escaped_message = ChatToLatex.escape(message.content_text)
      return "\\chatincoming{" + message.timestamp.strftime("%H:%M") + " " + message.sender + "}{" + generate_color_tag(message) + "}{ " + escaped_message + "}"
    end

    def flow_new_day(message)
      return "\\newday{" + message.timestamp.strftime("%d/%m/%Y") + "}"
    end

    def track_progress(count)
      # update prgress every 100 messages
      if count % 100 == 0
        progress = ((count * 100 ) / @chat_message_count)
        @chat_design.update(processing_progress: progress)
      end
    end
  end
end
class BaseService
  # basic structure of the services:
  # defines success and error functions that are 
  # called in the services to build the result object

  attr_accessor :user

  class Result < Struct.new(:subject, :error)
    def success?
      error.nil?
    end
  end

  def initialize(user = nil)
    @user = user
  end

  private
  def success(subject = {})
    @result = Result.new(subject)
    @result
  end

  def error(err, subject = {})
    @result = Result.new(subject, err)
    @result
  end
end
# frozen_string_literal: true

require 'rubygems'
require 'zip'

module Files
  class ZipTexService < ::BaseService
    def initialize(chat_design, user)
      super(user)
      @chat_design = chat_design
    end

    # this service generates a zip file of all required files inMemory
    def execute
      # wrong argument handling
      raise 'Design not found' unless @chat_design.present?
      raise 'Tex File not built yet' unless @chat_design.tex_file.attached?

      # collect files to be zipped
      @tex_file = @chat_design.tex_file.download
      @base_class = File.open('latex/base.cls', 'r')
      @advanced_class = File.open('latex/' + @chat_design.page_outline + '.cls', 'r')
      @bg_image = File.open('latex/backgrounds/' + @chat_design.bg_image, 'r') if @chat_design.bg_image.present?

      # write files to zip filestream
      @zip_file = Zip::OutputStream.write_buffer do |zip|
        zip.put_next_entry 'chat.tex'
        zip.print @tex_file
        zip.put_next_entry 'latex/base.cls'
        zip.print @base_class.read
        zip.put_next_entry 'latex/' + @chat_design.page_outline + '.cls'
        zip.print @advanced_class.read
        if @bg_image.present?
          zip.put_next_entry 'latex/backgrounds/' + @chat_design.bg_image
          zip.print @bg_image.read
        end
      end

      # rewind and return filestream
      @zip_file.rewind

      success(@zip_file)
    rescue StandardError => exception
      error(exception)
    end
  end
end

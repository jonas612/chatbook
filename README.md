# README
[![pipeline status](https://wiwi-gitlab.uni-muenster.de/m_heuc03/chatbook/badges/master/pipeline.svg)](https://wiwi-gitlab.uni-muenster.de/m_heuc03/chatbook/commits/master)

## Introduction
Chatbook is an Application that provides the service of transforming chat-histories
from WhatsApp and Facebook Messenger into beautiful and highly customizable 
formats, downloadable as PDF-file. The app uses LaTeX to compile the PDF-files
and is further able to provide the source .tex files for even more customization
options using an external LaTeX compiler.

The app can either be ran locally or be deployed on a webserver.

## Usage
### Setup local environment

#### Prerequisites
* Installation of `docker` and `docker-compose` for running the app
* Installation of `git` for cloning the repository

#### Setup
The following steps are necessary to run the app locally:

1. Clone the app into a folder of your choice
2. Start the Application by executing 
   `docker-compose up`
   The first initialization may take some time, since texlive is installed 
   inside the docker container. Keep the command line open as long as you 
   want to use the software.
3. Visit `localhost:4000` in your browser
4. Login with the local user account `localuser@latex-chatbook.de`, 
   password `local12345`. You can add additional user via Register Page if 
   necessary.
5. Get started and import a chat.
6. To close the software press CRTL-C in the command line window. Once all 
   container have stopped you exit the window by executing the command `exit`

Note that all your uploaded data is still saved inside the program and can be 
reused on the next execution. In order to delete certain upload, click on the
delete button in the chat upload overview. If you want to wipe all the data 
and start with a fresh application execute `docker-compose down` in a command
line window from the software's root directory. To completely remove the 
application delete the folder from your computer. After deleting the folder you
should delete created containers and images from docker by running `docker 
system prune -a`. Please note that if you use docker for other application, this 
command will also delete unused containers, images, and volumes from these 
programs. In that case you might want to remove them manually. A documentation 
can be found [here](https://docs.docker.com/engine/reference/commandline/rm/).

You can easily deploy this application to a webserver by following the 
instructions above and using `docker-compose-production.yml`.


## Development

### Setup
In order to contribute to this app, you have to setup a local development 
environment by executing the following steps.

1. Clone the app into a folder of your choice
2. Copy the example `.env` file with `cp .example.env .env`
3. Build the docker image using 
   `docker-compose build`
4. Start the Application in dev-mode by executing 
   `docker-compose up`
5. Initialize the database by running 
   `docker-compose run app exec bundle exec rake db:create db:migrate`
   and make sure the process finishes without errors.
6. Visit `localhost:3000` in your browser

### Make manual database corrections
For the unlikely case of databank corruptions you can try to fix broken entries
using `pg-admin`. `pg-admin` automatically starts with the app an is accessible 
via `localhost:5050`. The steps are:

1. Visit `localhost:5050`
2. Login to `pg-admin` with user `admin` and password `admin` 
   (if not configured differently)
3. Create a new database connection by clicking on `Add server`
4. Enter credentials user `postgres` and password `postgres`
   (if not configured differently)
5. Find the database information on the left side of the window

For more information consult the pg-admin4 documentation.

### Database clear
Follow these steps to completely wipe the database.

1. Delete the directory `db` from the folder `temp`
2. Execute `docker-compose down`
3. Execute `docker-compose up` and start with a fresh app

### Run tests locally (for development)
To run SPEC files locally for development purposes use these steps:
1. Start the application with `docker-compose up`
2. Start the RSpec-guard by running `docker-compose app exec bundle exec guard`
3. Run all test by hitting `<Enter>` run specific tests by saving the SPEC file


Background pictures are sourced from [freepic.com]("https://freepik.com").
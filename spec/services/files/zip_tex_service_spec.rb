# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Files::ZipTexService do
  before do
    user = User.create(email: 'invalid@invalid.de', password: 'password')
    user.save
    @chat_upload = ChatUpload.create(user: user, name: 'TEST', messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
    @chat_upload.save!
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user1', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user2', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user3', content_text: 'TEST')
    @chat_design = ChatDesign.new(
      chat_upload: @chat_upload,
      status: 'active',
      name: 'TEST',
      my_user_name: 'user1',
      page_size: :a4,
      page_outline: :flow_design,
      column_count: 2,
      color_set: :bootstrap,
      font: :fira_sans,
      tex_file: Rack::Test::UploadedFile.new(File.open('spec/fixtures/chat.tex')),
      bg_image: 'leaves.pdf'
    )
    @chat_design.save!
  end

  it 'does not throw an error' do
    result = Files::ZipTexService.new(@chat_design, @user).execute
    expect(result.success?).to be_truthy
  end

  it 'creates an in-memory zip file' do
    result = Files::ZipTexService.new(@chat_design, @user).execute
    expect(result.subject).to be_a(StringIO)
  end

  it 'does throw an error for missing design' do
    result = Files::ZipTexService.new(nil, @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does throw an error for missing tex file' do
    @chat_design.tex_file.purge
    result = Files::ZipTexService.new(@chat_design, @user).execute
    expect(result.success?).to be_falsy
  end
end

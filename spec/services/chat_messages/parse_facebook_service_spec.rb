# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatMessages::ParseWhatsappService do
  before do
    @user = User.create(email: 'invalid@invalid.de', password: 'password')
    @user.save!
    @chat_upload = ChatUpload.create(user: @user, name: 'TEST', messenger_type: :fbmessenger, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/facebook_fixture.json')))
    @chat_upload.save!
  end

  it 'does not throw an error' do
    result = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    expect(result.success?).to be_truthy
  end

  it 'does throw an error for wrong messenger_type' do
    @chat_upload.messenger_type = :whatsapp
    result = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does throw an error for wrong file_format' do
    @chat_upload.file_raw.attach(Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
    result = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does save the correct number of messages' do
    result = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    subject = ChatMessage.all
    expect(subject.count).to be(4)
  end

  it 'does identify message parts correctly' do
    result = ChatMessages::ParseFacebookService.new(@chat_upload, @user).execute
    expect(result.success?).to be_truthy
    subject = ChatMessage.first
    expect(subject.sender).to eql('Facebook User')
    expect(subject.content_text).to eql("Hahah I see, at least I'm not the only one in a shitty situation at the moment 😁")
    timestamp = Time.strptime('1529314649584', '%Q')
    expect(subject.timestamp).to eql(timestamp)
  end
end

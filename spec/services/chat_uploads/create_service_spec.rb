# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatUploads::CreateService do
  before do
    @user = User.create(email: 'invalid@invalid.de', password: 'password')
    @user.save!
    @params = { name: 'TEST', description: 'TESTDESCRIPTION', messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')) }
    # allow_any_instance_of(UploadCreateWorker).to receive(:perform)
    UploadCreateWorker.stub(:perform_async)
  end

  it 'does not throw an error for correct formats' do
    result = ChatUploads::CreateService.new(@params, @user).execute
    expect(result.success?).to be_truthy
  end

  it 'does throw an error for missing name' do
    result = ChatUploads::CreateService.new(@params.merge(name: ''), @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does throw an error for missing messenger_type' do
    result = ChatUploads::CreateService.new(@params.merge(messenger_type: ''), @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does throw an error for missing file' do
    result = ChatUploads::CreateService.new(@params.merge(file_raw: ''), @user).execute
    expect(result.success?).to be_falsy
  end

  it 'does not throw an error for missing description' do
    result = ChatUploads::CreateService.new(@params.merge(description: ''), @user).execute
    expect(result.success?).to be_truthy
  end

  it 'saves data to the database correctly' do
    result = ChatUploads::CreateService.new(@params, @user).execute
    subject = ChatUpload.first
    expect(subject.name).to eql('TEST')
    expect(subject.messenger_type).to eql('whatsapp')
    expect(subject.file_raw.attached?).to be_truthy
  end

  it 'executes the upload_create_worker' do
    UploadCreateWorker.should_receive(:perform_async)
    # expect(UploadCreateWorker).to receive(:perform_async)
    result = ChatUploads::CreateService.new(@params, @user).execute
    expect(result.success?).to be_truthy
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Latex::CreateService do
  before do
      user = User.create(email:"invalid@invalid.de", password:"password")
      user.save
      @chat_upload = ChatUpload.create(user: user, name: "TEST", messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
      @chat_upload.save!
      @chat_upload.chat_messages.create(timestamp: Date.new(2001,2,3), sender:"user1",content_text:"TEST")
      @chat_upload.chat_messages.create(timestamp: Date.new(2001,2,3), sender:"user2",content_text:"😄")
      @chat_upload.chat_messages.create(timestamp: Date.new(2001,2,3), sender:"user3",content_text:"https://chatbook.uni-muenster.de")
      @chat_upload.chat_messages.create(timestamp: Date.new(2001,2,3), sender:"user4",content_text:"\\s#$%_{}~&")
      @chat_design = ChatDesign.new(
        chat_upload: @chat_upload, 
        status: "active", 
        name: "TEST", 
        my_user_name: "user1", 
        page_size: :a4,
        page_outline: :flow_design,
        column_count: 2,
        color_set: :bootstrap,
        font: :fira_sans,
        bg_image: "leaves.pdf"
        )
      @chat_design.save!
  end

  it "does not throw an error for flow design" do
    result = Latex::CreateService.new(@chat_design, @user).execute
    expect(result.success?).to be_truthy
  end

  it "does not throw an error for table design" do
    @chat_design.page_outline = :table_design
    result = Latex::CreateService.new(@chat_design, @user).execute
    expect(result.success?).to be_truthy
  end

  it "does not throw an error for speechbubble design" do
    @chat_design.page_outline = :bubbles_design
    result = Latex::CreateService.new(@chat_design, @user).execute
    expect(result.success?).to be_truthy
  end

  describe "latex file contents" do
    before do
      result = Latex::CreateService.new(@chat_design, @user).execute
      @tex_file = result.subject.tex_file.download
    end

    it "creates basic document correctly" do
      expect(@tex_file).to include "\\graphicspath{{latex/}}\n"
      expect(@tex_file).to include "\\begin{document}\n"
      expect(@tex_file).to include "\\end{document}"
    end

    it "evaluates page_size correctly for a4" do
      expect(@tex_file).to include "\\usepackage[a4paper, total={7in, 10in}]{geometry}\n"
    end

    it "evaluates page_outline correctly" do
      expect(@tex_file).to include "\\documentclass{flow_design}\n"
    end

    it "evaluates column_count correctly correctly" do
      expect(@tex_file).to include "\\begin{multicols}{2}\n"
      expect(@tex_file).to include "\\end{multicols}\n"
    end

    it "evaluates font correctly correctly" do
      expect(@tex_file).to include "\\usepackage[sfdefault]{FiraSans}\n"
    end

    it "evaluates bg_image correctly correctly" do
      expect(@tex_file).to include "\\usepackage{eso-pic}\n"
      expect(@tex_file).to include "\\AddToShipoutPictureBG{%\n"
      expect(@tex_file).to include "\\AtPageLowerLeft{\\includegraphics[width=\\paperwidth,height=\\paperheight]{backgrounds/leaves.pdf}"
    end

    describe "message content" do
      it "evaluates test message correctly" do
        # combination of colorset and chat_message content
        expect(@tex_file).to include "\\chatoutgoing{00:00 user1}{bootstrap_outgoing}{TEST}\n"
      end

      it "evaluates test message with emoji correctly" do
        # combination of colorset and chat_message content
        expect(@tex_file).to include "\\chatincoming{00:00 user2}{bootstrap_incoming_0}{ \\emoji{1F604}}\n"
      end

      it "evaluates test message with url correctly" do
        # combination of colorset and chat_message content
        expect(@tex_file).to include "\\chatincoming{00:00 user3}{bootstrap_incoming_1}{ \\url{https://chatbook.uni-muenster.de}}\n"
      end

      it "evaluates test message with reserved characters correctly" do
        # combination of colorset and chat_message content
        expect(@tex_file).to include "\\chatincoming{00:00 user4}{bootstrap_incoming_2}{ s\\#\\$\\%\\_\\{\\}\\~and}\n"
      end
    end
  end
end
# frozen_string_literal: true

require 'rails_helper'

# the compile service in not tested in the CI, because setting up TexLive 
# for every pipeline is too resource intense
RSpec.describe Latex::CompileService, type: 'latex-compile' do
  before do
    user = User.create(email: 'invalid@invalid.de', password: 'password')
    user.save
    @chat_upload = ChatUpload.create(user: user, name: 'TEST', messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
    @chat_upload.save!
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user1', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user2', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user3', content_text: 'TEST')
    @chat_design = ChatDesign.new(
      chat_upload: @chat_upload,
      status: 'active',
      name: 'TEST',
      my_user_name: 'user1',
      page_size: :a4,
      page_outline: :flow_design,
      column_count: 2,
      color_set: :bootstrap,
      font: :fira_sans,
      tex_file: Rack::Test::UploadedFile.new(File.open('spec/fixtures/chat.tex'))
    )
    @chat_design.save!
  end

  it 'does not throw an error' do
    result = Latex::CompileService.new(@chat_design, @user).execute
    expect(result.success?).to be_truthy
  end

  it 'does throw an error for invalid latex files' do
    @chat_design.tex_file.attach(Rack::Test::UploadedFile.new(File.open('spec/fixtures/chat.invalid.tex')))
    result = Latex::CompileService.new(@chat_design, @user).execute
    expect(result.success?).to be_falsy
  end

  it 'builds the pdf file correctly' do
    result = Latex::CompileService.new(@chat_design, @user).execute
    text_analysis = PDF::Inspector::Text.analyze(result.subject)
    pdf_content = text_analysis.strings.join('')

    expect(pdf_content).to include 'Hi,howareyou?'
  end
end

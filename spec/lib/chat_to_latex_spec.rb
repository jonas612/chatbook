require 'rails_helper'

RSpec.describe ChatToLatex do

  it "escapes reseved characters" do
    expect(ChatToLatex.escape("#")).to eql("\\#")
    expect(ChatToLatex.escape("$")).to eql("\\$")
    expect(ChatToLatex.escape("%")).to eql("\\%")
    expect(ChatToLatex.escape("_")).to eql("\\_")
    expect(ChatToLatex.escape("{")).to eql("\\{")
    expect(ChatToLatex.escape("}")).to eql("\\}")
    expect(ChatToLatex.escape("~")).to eql("\\~")
    expect(ChatToLatex.escape("&")).to eql(I18n.t("and", scope: :dict))
    expect(ChatToLatex.escape("\"")).to eql("''")
    expect(ChatToLatex.escape("\„")).to eql("''")
  end

  it "deletes unprocessable characters" do
    expect(ChatToLatex.escape("\\")).to eql("")
    expect(ChatToLatex.escape("^")).to eql("")
  end

  it "escapes URLs" do
    expect(ChatToLatex.escape("http://chatbook.uni-muenster.de")).to eql("\\url{http://chatbook.uni-muenster.de}")
    expect(ChatToLatex.escape("https://chatbook.uni-muenster.de")).to eql("\\url{https://chatbook.uni-muenster.de}")
  end

  describe "emojis" do
    it "escapes emojis with a single codepoint" do
      expect(ChatToLatex.escape("😆")).to eql("\\emoji{1F606}")
      expect(ChatToLatex.escape("🍎")).to eql("\\emoji{1F34E}")
      expect(ChatToLatex.escape("🏉")).to eql("\\emoji{1F3C9}")
    end

    it "escapes emojis with multiple codepoints" do
      expect(ChatToLatex.escape("👍🏿")).to eql("\\emoji{1F44D-1F3FF}")
      expect(ChatToLatex.escape("👨‍👩‍👧‍👦")).to eql("\\emoji{1F468-200D-1F469-200D-1F467-200D-1F466}")
      expect(ChatToLatex.escape("👨🏽‍")).to eql("\\emoji{1F468-1F3FD}‍")
    end

    it "ignores unsupported emojis" do
      expect(ChatToLatex.escape("🥶")).to eql("\u{1F976}")
      expect(ChatToLatex.escape("🥰")).to eql("\u{1F970}")
      expect(ChatToLatex.escape("🤯")).to eql("🤯")
    end

    it "escapes facebook thumb emoji" do
      expect(ChatToLatex.escape("\u{f0000}")).to eql("\\emoji{1F44D}")
    end
  end
end
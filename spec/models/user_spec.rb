# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  # user model was imported from devise, no explicit testing
end

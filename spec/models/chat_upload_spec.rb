# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatUpload, type: :model do
  describe 'model methods' do
    before do
      user = User.create(email: 'invalid@invalid.de', password: 'password')
      user.save
      @chat_upload = ChatUpload.create(user: user, name: 'TEST', messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
      @chat_upload.save!
      @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user1', content_text: 'TEST')
      @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user2', content_text: 'TEST')
      @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user3', content_text: 'TEST')
    end

    it 'checks for groupchats correctly' do
      expect(@chat_upload.groupchat?).to be_truthy
    end

    it 'identifies chat participants' do
      expect(@chat_upload.participants.length).to be(3)
    end

    it 'counts message number correcty' do
      expect(@chat_upload.message_count).to be(3)
    end
  end
end

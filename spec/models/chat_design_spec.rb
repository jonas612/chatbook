# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatDesign, type: :model do
  before do
    user = User.create(email: 'invalid@invalid.de', password: 'password')
    user.save
    @chat_upload = ChatUpload.create(user: user, name: 'TEST', messenger_type: :whatsapp, file_raw: Rack::Test::UploadedFile.new(File.open('spec/fixtures/whatsapp_fixture.txt')))
    @chat_upload.save!
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user1', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user2', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 3), sender: 'user3', content_text: 'TEST')
    @chat_upload.chat_messages.create(timestamp: Date.new(2001, 2, 4), sender: 'user4', content_text: 'TEST')
    @chat_design = ChatDesign.new(
      chat_upload: @chat_upload,
      status: 'active',
      name: 'TEST',
      my_user_name: 'user1',
      page_size: :a4,
      page_outline: :flow_design,
      column_count: 2,
      color_set: :bootstrap,
      font: :fira_sans
    )
  end

  it 'saves a valid chat design' do
    expect(@chat_design.save).to be_truthy
  end

  describe 'partial validation in wizard' do
    context 'design creation' do
      before do
        @partial_chat_design = ChatDesign.new(
          chat_upload: @chat_upload,
          status: 'create_design'
        )
      end

      it 'validates the presence of name for existing value' do
        @partial_chat_design.name = 'TEST DESIGN'
        expect(@partial_chat_design.save).to be_truthy
      end

      it 'validates the presence of name for missing value' do
        @partial_chat_design.name = ''
        expect(@partial_chat_design.save).to be_falsy
      end
    end

    context 'chat configuration' do
      before do
        @partial_chat_design = ChatDesign.new(
          chat_upload: @chat_upload,
          name: 'TEST DESIGN',
          status: 'basic_configuration'
        )
      end

      it 'validates the presence of my_user_name for exisitng value' do
        @partial_chat_design.my_user_name = 'TEST USER'
        expect(@partial_chat_design.save).to be_truthy
      end

      it 'validates the presence of my_user_name for missing value' do
        @partial_chat_design.my_user_name = ''
        expect(@partial_chat_design.save).to be_falsy
      end

      it 'validates the presence of messages in the selected period for exisitng messages' do
        @partial_chat_design.my_user_name = 'TEST USER'
        @partial_chat_design.period_from = Date.new(2001, 2, 2)
        @partial_chat_design.period_to = Date.new(2001, 2, 4)
        expect(@partial_chat_design.save).to be_truthy
      end

      it 'validates the presence of messages in the selected period for missing messages' do
        @partial_chat_design.my_user_name = 'TEST USER'
        @partial_chat_design.period_from = Date.new(2001, 2, 5)
        @partial_chat_design.period_to = Date.new(2001, 2, 6)
        expect(@partial_chat_design.save).to be_falsy
      end
    end

    context 'style configuration' do
      before do
        @partial_chat_design = ChatDesign.new(
          chat_upload: @chat_upload,
          name: 'TEST DESIGN',
          my_user_name: 'TEST USER',
          status: 'style_configuration'
        )
      end

      it 'validates the presence of design_parameter for exisitng values' do
        @partial_chat_design.update(
          page_size: :a4,
          page_outline: :flow_design,
          column_count: 2,
          color_set: :bootstrap,
          font: :fira_sans
        )
        expect(@partial_chat_design.save).to be_truthy
      end

      it 'validates the presence of design_parameter for missing values' do
        expect(@partial_chat_design.save).to be_falsy
      end
    end
  end

  describe 'messages in selected period' do
    it 'returns the correct number of messages for a period' do
      @chat_design.period_from = Date.new(2001, 2, 2)
      @chat_design.period_to = Date.new(2001, 2, 4)
      expect(@chat_design.chat_messages_in_period.count).to be(4)
    end

    it 'returns the correct number of messages for a smaller period' do
      @chat_design.period_from = Date.new(2001, 2, 4)
      @chat_design.period_to = Date.new(2001, 2, 4)
      expect(@chat_design.chat_messages_in_period.count).to be(1)
    end

    it 'returns the correct number of messages for open start period' do
      @chat_design.period_to = Date.new(2001, 2, 3)
      expect(@chat_design.chat_messages_in_period.count).to be(3)
    end

    it 'returns the correct number of messages for open end period' do
      @chat_design.period_from = Date.new(2001, 2, 4)
      expect(@chat_design.chat_messages_in_period.count).to be(1)
    end

    it 'sorts messages correctly for whatsapp (created_at)' do
      @chat_upload.chat_messages.destroy_all
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00), sender: 'user1', content_text: 'message1')
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00), sender: 'user2', content_text: 'message2')
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00), sender: 'user3', content_text: 'message3')
      expect(@chat_design.chat_messages_in_period.first.content_text).to eql('message1')
    end

    it 'sorts messages correctly for fbmessenger (timestamp)' do
      @chat_upload.messenger_type = :fbmessenger
      @chat_design.chat_upload.chat_messages.destroy_all
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00, 40), sender: 'user1', content_text: 'message3')
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00, 35), sender: 'user2', content_text: 'message2')
      @chat_upload.chat_messages.create(timestamp: Time.new(2001, 2, 3, 10, 00, 20), sender: 'user3', content_text: 'message1')
      expect(@chat_design.chat_messages_in_period.first.content_text).to eql('message1')
    end
  end

  describe 'background images file list' do
    it 'fetches background images from the latex folder' do
      expect(ChatDesign.get_background_images).to_not be_empty
    end

    it 'contains at least one pdf file' do
      expect(ChatDesign.get_background_images.last[-4, 4]).to eql('.pdf')
    end
  end
end

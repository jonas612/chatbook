require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ChatDesignsHelper. For example:
#
# describe ChatDesignsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ChatDesignsHelper, type: :helper do
  # no functionality implemented
end

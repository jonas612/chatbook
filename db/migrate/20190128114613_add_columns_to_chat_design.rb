class AddColumnsToChatDesign < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_designs, :processing_status, :integer, default: 0
    add_column :chat_designs, :processing_error, :string
  end
end

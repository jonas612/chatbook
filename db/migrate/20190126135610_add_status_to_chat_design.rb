class AddStatusToChatDesign < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_designs, :status, :string, default: "inactive"
  end
end

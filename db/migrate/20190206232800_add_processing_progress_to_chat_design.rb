class AddProcessingProgressToChatDesign < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_designs, :processing_progress, :integer
  end
end

class AddPdfFileToChatDesigns < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_designs, :pdf_file, :string
  end
end

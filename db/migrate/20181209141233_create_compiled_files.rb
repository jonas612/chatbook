class CreateCompiledFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :compiled_files do |t|
      t.timestamps
    end
  end
end

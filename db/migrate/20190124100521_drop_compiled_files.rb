class DropCompiledFiles < ActiveRecord::Migration[5.2]
  def up
    drop_table :compiled_files
  end

  def down
    create_table :compiled_files do |t|
      t.timestamps
    end
  end
end

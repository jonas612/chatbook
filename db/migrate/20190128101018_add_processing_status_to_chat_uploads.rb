class AddProcessingStatusToChatUploads < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_uploads, :processing_status, :integer, default: 0
  end
end

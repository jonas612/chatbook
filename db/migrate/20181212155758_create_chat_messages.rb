class CreateChatMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_messages do |t|
      t.string :sender
      t.datetime :timestamp
      t.text :content_text
      t.references :chat_upload, foreign_key: true

      t.timestamps
    end
  end
end

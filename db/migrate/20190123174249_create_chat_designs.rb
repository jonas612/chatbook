class CreateChatDesigns < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_designs do |t|
      t.string :name
      t.string :my_user_name
      t.datetime :period_from
      t.datetime :period_to
      t.integer :page_size
      t.integer :page_outline
      t.integer :column_count
      t.integer :color_set
      t.integer :font
      t.string :bg_image

      t.references :chat_upload, foreign_key: true

      t.timestamps
    end
  end
end

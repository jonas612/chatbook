class AddMsgTypeToChatMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_messages, :msg_type, :integer
  end
end

class AddTexFileToChatDesigns < ActiveRecord::Migration[5.2]
  def change
    add_column :chat_designs, :tex_file, :string
  end
end

class CreateChatUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_uploads do |t|
      t.string :name
      t.text :description
      t.integer :messenger_type
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

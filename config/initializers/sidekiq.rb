# frozen_string_literal: true

require 'sidekiq'
require 'sidekiq/web'

sidekiq_config = { url: ENV['JOB_WORKER_URL'] }
sidekiq_user = ENV['SIDEKIQ_USERNAME']
sidekiq_password = ENV['SIDEKIQ_PASSWORD']

Sidekiq.configure_server do |config|
  config.redis = sidekiq_config
end

Sidekiq.configure_client do |config|
  config.redis = sidekiq_config
end

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [sidekiq_user, sidekiq_password]
end

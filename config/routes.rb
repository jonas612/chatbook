# frozen_string_literal: true

require 'sidekiq/web'


Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'static_pages#index'

  authenticate :user do
    # upload, design and wizard
    resources :chat_uploads, only: %i[index new create destroy show] do
      resources :chat_designs, only: %i[index new create edit update destroy] do
        resources :wizard, controller: 'chat_designs/wizard', only: [:show, :create, :update]
        get 'wizards/', to: "chat_designs/wizard#create"
        get 'wizard/style_configuration/refresh_preview/', to: 'chat_designs/wizard#refresh_preview'
      end
      get 'chat_designs/:id/build_files/', to: 'chat_designs#build_files', as: 'chat_design_build_files'
      get 'chat_designs/:id/processing/', to: 'chat_designs#processing', as: 'chat_design_processing'
    end
    get 'chat_upload/:id/processing/', to: 'chat_uploads#processing', as: 'chat_upload_processing'

    # file download
    get '/files/tex_information/:chat_design_id', to: 'files#tex_information', as: 'file_information_tex'
    get '/files/download_tex/:chat_design_id', to: 'files#download_tex', as: 'file_download_tex'
    get '/files/download_pdf/:chat_design_id', to: 'files#download_pdf', as: 'file_download_pdf'

    # static pages
    get '/export_help/', to: 'static_pages#export_help', as: 'static_page_export_help'

  end

  # sidekiq ui
  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end

  # public routes
  get '/imprint/', to: 'static_pages#imprint', as: 'imprint'
  get '/privacy_policy/', to: 'static_pages#privacy_policy', as: 'privacy_policy'

end

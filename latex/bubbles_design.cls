\ProvidesClass{bubbles_design}[2016/22/10 version 1.00 Script]
% Adapted from
% https://tex.stackexchange.com/questions/270252/boxes-for-sms-conversation
\LoadClass{base}

\usepackage[many]{tcolorbox}
\usetikzlibrary{shadows}

\def\BoxP{4pt}

\newtcbox{chatoutgoing}[2]{
  enhanced,
  % interior code={},
  frame code={
    \coordinate (aux1) at ([shift={(5pt,0pt)}]frame.north east);
    \filldraw[#2]
      (aux1) to[]
      ([shift={(0pt,-7pt)}]frame.north east) -- 
      ([shift={(-4pt,0pt)}]frame.north east)
      to[] (aux1);
    \filldraw[#2]
      ([shift={(0pt,\BoxP)}]frame.south east) --
      ([shift={(0pt,-\BoxP)}]frame.north east) to[out=90,in=0]
      ([shift={(-\BoxP,0pt)}]frame.north east) --
      ([shift={(\BoxP,0pt)}]frame.north west) to[out=180,in=90]
      ([shift={(0pt,-\BoxP)}]frame.north west) --
      ([shift={(0pt,\BoxP)}]frame.south west) to[out=-90,in=180]
      ([shift={(\BoxP,0pt)}]frame.south west) --
      ([shift={(-\BoxP,0pt)}]frame.south east) to [out=0,in=0]
      ([shift={(-1pt,4.5pt)}]frame.south east);},
  colback=#2,
  colframe=#2,
  width=0.75\columnwidth,
  %bottom=0pt,
  capture=minipage,
  flush right,
  enlarge right by=4pt,
  %fontupper=\sffamily,
  attach boxed title to top right,
  coltitle=black,
  %fonttitle=\sffamily,
  boxed title style={size=small,frame empty,interior empty},
  title=#1,
  left=2pt,
  right=2pt
}

\newtcbox{chatincoming}[2]{
  enhanced,
  % interior code={},
  frame code={
    \coordinate (aux1) at ([shift={(-5pt,0pt)}]frame.north west);
    \filldraw[#2]
      (aux1) to[]
      ([shift={(0pt,-7pt)}]frame.north west) --
      ([shift={(4pt,0pt)}]frame.north west)
      to (aux1);
    \filldraw[#2]
      ([shift={(0pt,\BoxP)}]frame.south west) --
      ([shift={(0pt,-\BoxP)}]frame.north west) to[out=90,in=180]
      ([shift={(\BoxP,0pt)}]frame.north west) --
      ([shift={(-\BoxP,0pt)}]frame.north east) to[out=0,in=90]
      ([shift={(0pt,-\BoxP)}]frame.north east) --
      ([shift={(0pt,\BoxP)}]frame.south east) to[out=-90,in=0]
      ([shift={(-\BoxP,0pt)}]frame.south east) --
      ([shift={(\BoxP,0pt)}]frame.south west) to[out=180,in=180]
      ([shift={(1pt,4.5pt)}]frame.south west);
  },
  colback=#2,
  colframe=#2,
  width=0.75\columnwidth,
  %bottom=0pt,
  capture=minipage,
  flush left,
  enlarge left by=4pt,
  attach boxed title to top left,
  coltitle=black,
  %fonttitle=\sffamily,
  boxed title style={size=small,frame empty,interior empty},
  title=#1,
  left=2pt,
  right=2pt
}
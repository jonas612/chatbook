\ProvidesClass{base}[2016/22/10 version 1.00 Script]
\LoadClass{article}

% encoding
\usepackage[utf8]{inputenc}

% -- remove unprocessable utf-8 characters (e.g. from facebook / unsupported emojis) --
% adapted from 
% https://tex.stackexchange.com/questions/307673/tell-latex-to-ignore-all-unicode-errors
\makeatletter
\def\UTFviii@defined#1{%
  \ifx#1\relax
    %
  \else\expandafter
    #1%
  \fi
}
\makeatother
% ----------------------------------------------------------------

% font
\usepackage[T1]{fontenc}

% emojis
\usepackage[ios]{emoji}

% urls
\usepackage{hyperref}
% allow linebreaks after every character in urls
% adapted from 
% https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url
\expandafter\def\expandafter\UrlBreaks\expandafter{\UrlBreaks
  \do\a\do\b\do\c\do\d\do\e\do\f\do\g\do\h\do\i\do\j%
  \do\k\do\l\do\m\do\n\do\o\do\p\do\q\do\r\do\s\do\t%
  \do\u\do\v\do\w\do\x\do\y\do\z\do\A\do\B\do\C\do\D%
  \do\E\do\F\do\G\do\H\do\I\do\J\do\K\do\L\do\M\do\N%
  \do\O\do\P\do\Q\do\R\do\S\do\T\do\U\do\V\do\W\do\X%
  \do\Y\do\Z}

% outline
\usepackage{longtable}
\usepackage{multicol}
\setlength{\columnseprule}{1pt}

% color and pictures
\usepackage{xcolor}
\usepackage{graphicx}

% colorset whatsapp
\definecolor{whatsapp_outgoing}{RGB}{186,233,95}
\definecolor{whatsapp_incoming_0}{RGB}{225,225,225}
\definecolor{whatsapp_incoming_1}{RGB}{225,225,225}
\definecolor{whatsapp_incoming_2}{RGB}{225,225,225}
\definecolor{whatsapp_incoming_3}{RGB}{225,225,225}

% colorset pastel
\definecolor{pastel_outgoing}{RGB}{186,255,201}
\definecolor{pastel_incoming_0}{RGB}{255,179,186}
\definecolor{pastel_incoming_1}{RGB}{255,223,186}
\definecolor{pastel_incoming_2}{RGB}{255,255,186}
\definecolor{pastel_incoming_3}{RGB}{186,225,255}

% colorset bright
\definecolor{bright_outgoing}{RGB}{229,68,76}
\definecolor{bright_incoming_0}{RGB}{255,142,28}
\definecolor{bright_incoming_1}{RGB}{255,247,70}
\definecolor{bright_incoming_2}{RGB}{116,215,253}
\definecolor{bright_incoming_3}{RGB}{255,185,252}

% colorset bootstrap
\definecolor{bootstrap_outgoing}{RGB}{217,83,79}
\definecolor{bootstrap_incoming_0}{RGB}{210,210,210}
\definecolor{bootstrap_incoming_1}{RGB}{91,192,222}
\definecolor{bootstrap_incoming_2}{RGB}{92,184,92}
\definecolor{bootstrap_incoming_3}{RGB}{66,139,202}

% colorset instragram
\definecolor{instagram_outgoing}{RGB}{150,104,66}
\definecolor{instagram_incoming_0}{RGB}{244,71,71}
\definecolor{instagram_incoming_1}{RGB}{238,220,49}
\definecolor{instagram_incoming_2}{RGB}{127,219,106}
\definecolor{instagram_incoming_3}{RGB}{14,104,206}

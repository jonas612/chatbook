\ProvidesClass{flow_design}[2016/22/10 version 1.00 Script]
\LoadClass{base}

\usepackage{xparse}

% Adapted from 
% https://tex.stackexchange.com/questions/30973/how-do-i-insert-a-border-below-text
\NewDocumentCommand{\myrule}{O{1pt} O{3pt} O{black}}{
  \par\nobreak
  \kern\the\prevdepth
  \kern#2
  {\color{#3}\hrule height #1 width\hsize}
  \kern#2
  \nointerlineskip
}

\newcommand{\chatincoming}[3]{
    \noindent\textcolor{#2}{\textbf{#1}}: #3\\
}

\newcommand{\chatoutgoing}[3]{
    \noindent\textcolor{#2}{\textbf{#1}}: #3\\
}

\newcommand{\newday}[1]{
  \leavevmode \\
  \begin{large}
  \indent #1
  \end{large}
  \myrule[1pt]
}